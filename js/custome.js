


/*===================================
    sidenav 
  ===================================*/

function openNav() {
    document.getElementById("mySidenav").style.width = "450px";
    document.body.style.backgroundColor = "rgba(168,232,226, 0.7)";
}
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "white";
}


function openNav2() {
    document.getElementById("rightside").style.width = "450px";
    document.body.style.backgroundColor = "rgba(168,232,226, 0.7)";
}
function closeNav2() {
    document.getElementById("rightside").style.width = "0";
    document.body.style.backgroundColor = "white";
}


function openNav3() {
    document.getElementById("rightside3").style.width = "450px";
    document.body.style.backgroundColor = "rgba(168,232,226, 0.7)";
}
function closeNav3() {
    document.getElementById("rightside3").style.width = "0";
    document.body.style.backgroundColor = "white";
}


function clickSize() {
    document.getElementById("size-content").style.display = "block";
}
function closeSize() {
    document.getElementById("size-content").style.display = "none";
}
function clickColor() {
    document.getElementById("size-content1").style.display = "block";
}
function closeColor() {
    document.getElementById("size-content1").style.display = "none";
}

function addbag() {
    document.getElementById("showcheck").style.display = "block";
}


function custombtn() {
    document.getElementById("cusbox").style.display = "block";
}
function closeCus() {
    document.getElementById("cusbox").style.display = "none";
}

/*===================================
    navbar menu 
  ===================================*/





//===== product item slid =====//

$(document).ready(function () {

$('.listnewarr').owlCarousel({
    nav:true,
    loop:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    margin:10,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }

});


$('.trendlist').owlCarousel({
    nav:true,
    loop:true,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    margin:10,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }

});



$('.relateProd').owlCarousel({
    nav:true,
    loop:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    margin:10,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }

});


 $( ".owl-prev").html('<i class="fa fa-chevron-left"></i>');
 $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');




});

/*===================================

    scrollTop  menu effects

  ===================================*/

 $(window).scroll(function () {
        if ($(window).scrollTop() > 100) {
            $(".menubar").addClass('meffect');
        } else {
            $(".menubar").removeClass('meffect');
        }
    });




/*===================================

    megadropdown menu

  ===================================*/
$(function(){
    $('.dropdown').hover(function() {
        $(this).addClass('open');
    },
    function() {
        $(this).removeClass('open');
    });
});


/*===================================

    scrollTop 

  ===================================*/


$(document).ready(function(){
  
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
      $('.scrollToTop').fadeIn();
    } else {
      $('.scrollToTop').fadeOut();
    }
  });
  
  //Click event to scroll to top
  $('.scrollToTop').click(function(){
    $('html, body').animate({scrollTop : 0},800);
    return false;
  });
  
});


/*===================================

    Tooltip 

  ===================================*/


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});


/*===================================

    Range Slider 

  ===================================*/



// With JQuery
$("#ex2").slider({});



/*===================================

    Value minus and plus 

  ===================================*/



    $('.minus').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest('div').find('input');
    var value = parseInt($input.val());
    
    if (value > 1) {
        value = value - 1;
    } else {                                                                                                                                                                
        value = 1;
    }
    
      $input.val(value);
      $("#qnty").val(value);
     
    
    
    });
    
    $('.plus').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest('div').find('input');
    var value = parseInt($input.val());
    
    if (value < 100) {
            value = value + 1;
    } else {
        value =100;
    }
    
    $input.val(value);
    $("#qnty").val(value);
    
    });
    
    /// $('.like-btn').on('click', function() {
     // $(this).toggleClass('is-active');
    //  });



 

  




